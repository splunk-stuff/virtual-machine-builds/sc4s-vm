## Source blocks

# VirtualBox
source "virtualbox-iso" "sc4s-rhel" {
  boot_command           = "${var.rhel_boot_command}"
  boot_wait              = "${var.boot_wait_virtualbox}"
  cd_content             = "${local.rhel_cd_content}"
  cd_label               = "${var.rhel_cd_label}"
  cpus                   = "${var.cpus}"
  disk_size              = "${var.disk_size}"
  export_opts            = [
    "--manifest",
    "--vsys", "0"
    ]
  format                 = "ova"
  gfx_controller         = "vmsvga"
  gfx_vram_size          = "32"
  guest_additions_path   = "/tmp/VBoxGuestAdditions.iso"
  guest_os_type          = "RedHat_64"
  hard_drive_interface   = "sata"
  headless               = "${var.headless}"
  iso_checksum           = "${var.rhel_iso_checksum_type}:${var.rhel_iso_checksum}"
  iso_urls               = ["${var.rhel_iso_local}", "${var.rhel_iso_url}"]
  memory                 = "${var.memory}"
  output_directory       = "output/virtualbox/${local.rhel_vm_name}/"
  shutdown_command       = "echo ${var.build_password} | sudo -S shutdown -P now"
  ssh_handshake_attempts = "${var.ssh_handshake_attempts}"
  ssh_password           = "${var.build_password}"
  ssh_pty                = "${var.ssh_pty}"
  ssh_timeout            = "${var.ssh_timeout}"
  ssh_username           = "${var.build_username}"
  vboxmanage             = [
    [ "modifyvm", "{{ .Name }}", "--vrde", "off" ],
    [ "modifyvm", "{{ .Name }}", "--natpf1", "syslog_udp,udp,,514,,514" ],
    [ "modifyvm", "{{ .Name }}", "--natpf1", "syslog_tcp,tcp,,6514,,6514" ]
  ]
  vm_name                = "${local.rhel_vm_name}"
}

# VMware Workstation
source "vmware-iso" "sc4s-rhel" {
  boot_command           = "${var.rhel_boot_command}"
  boot_wait              = "${var.boot_wait_vmware}"
  cd_content             = "${local.rhel_cd_content}"
  cd_label               = "${var.rhel_cd_label}"
  disk_size              = "${var.disk_size}"
  guest_os_type          = "rhel8-64"
  headless               = "${var.headless}"
  iso_checksum           = "${var.rhel_iso_checksum_type}:${var.rhel_iso_checksum}"
  iso_urls               = ["${var.rhel_iso_local}", "${var.rhel_iso_url}"]
  memory                 = "${var.memory}"
  output_directory       = "output/vmware/${local.rhel_vm_name}/"
  shutdown_command       = "echo ${var.build_password} |sudo -S shutdown -P now"
  skip_compaction        = false
  ssh_handshake_attempts = "${var.ssh_handshake_attempts}"
  ssh_password           = "${var.build_password}"
  ssh_pty                = "${var.ssh_pty}"
  ssh_timeout            = "${var.ssh_timeout}"
  ssh_username           = "${var.build_username}"
  tools_upload_flavor    = "linux"
  vm_name                = "${local.rhel_vm_name}"
}

# ESXI
source "vsphere-iso" "sc4s-rhel" {
  // ESXI Config
  vcenter_server         = "${var.vsphere-iso_vcenter_server}"
  host                   = "${var.vsphere-iso_host}"
  username               = "${var.vsphere-iso_username}"
  password               = "${var.vsphere-iso_password}"
  insecure_connection    = "${var.vsphere-iso_insecure}"
  datastore              = "${var.vsphere-iso_datastore}"
  // VM Config
  boot_command           = "${var.rhel_boot_command}"
  boot_wait              = "${var.boot_wait_vsphere}"
  cd_content             = "${local.rhel_cd_content}"
  cd_label               = "${var.rhel_cd_label}"
  guest_os_type          = "rhel8_64Guest"
  iso_checksum           = "${var.rhel_iso_checksum_type}:${var.rhel_iso_checksum}"
  iso_urls               = ["${var.rhel_iso_local}", "${var.rhel_iso_url}"]
  network_adapters {
      network = "VM Network"
      network_card = "vmxnet3"
  }
  RAM                    = "${var.memory}"
  shutdown_command       = "echo ${var.build_password} |sudo -S shutdown -P now"
  ssh_handshake_attempts = "${var.ssh_handshake_attempts}"
  ssh_password           = "${var.build_password}"
  ssh_pty                = "${var.ssh_pty}"
  ssh_timeout            = "${var.ssh_timeout}"
  ssh_username           = "${var.build_username}"
  storage {
      disk_size = "${var.disk_size}"
      disk_thin_provisioned = true
  }
  vm_name                = "${local.rhel_vm_name}"
}
