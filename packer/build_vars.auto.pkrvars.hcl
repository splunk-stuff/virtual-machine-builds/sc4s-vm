###
### CentOS Stream 9 Build Vars
###

vm_name                  = "SC4S-VM"
build_username           = "sc4s"
build_password           = "changeme"
build_password_encrypted = "$6$1K8b58qoBWruO0PO$MSBkNkfx/vPDJWfO8c/seinsojiWOKePQUnUKYhFP/2VO6SKZ1luHyuuVIRezCFfH60TPJlW2I0Ps5pKbIZ1F1"

rhel_iso_url             = "http://mirror.stream.centos.org/9-stream/BaseOS/x86_64/iso/CentOS-Stream-9-latest-x86_64-boot.iso"
# manually update checksum if needed "http://mirror.stream.centos.org/9-stream/BaseOS/x86_64/iso/CentOS-Stream-9-latest-x86_64-boot.iso.SHA256SUM"
rhel_iso_checksum        = "c6fb2810e6c695476f45bfb8fec69a31bf82b2c1845274bb6f0baf9ec42878b3"

cpus   = 4
memory = 8192
