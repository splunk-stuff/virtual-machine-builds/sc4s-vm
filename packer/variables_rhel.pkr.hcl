###
## rhel Variables #
#

# Build
variable "rhel_cd_label" {
  description = "The label for the attached cd when booting."
  type = string
  default = "OEMDRV"
}

variable "rhel_boot_command" {
  description = "The boot_command is an array of strings. The strings are all typed in sequence."
  type = list (string)
  default = ["<tab> inst.ks=hd:/dev/sr1:/ks.cfg<enter>"]
}

# ISO
variable "rhel_iso_checksum" {
  description = "The checksum for the ISO file or virtual hard drive file."
  type = string
}

variable "rhel_iso_checksum_type" {
  description = "The type of checksum for the ISO file or virtual hard drive file."
  type = string
  default = "sha256"
}

variable "rhel_iso_local" {
  description = "The location of the ISO containing the installation image if stored locally."
  type = string
  default = ""
}

variable "rhel_iso_url" {
  description = "A URL to the ISO containing the installation image."
  type = string
  default = ""
}
