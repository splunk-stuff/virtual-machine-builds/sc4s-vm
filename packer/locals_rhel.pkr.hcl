## Locals

locals {

  rhel_cd_content = {
    "/ks.cfg" = templatefile("${abspath(path.root)}/templates/ks.cfg.pkrtpl.hcl", {
      hostname                 = "${var.vm_name}"
      build_username           = "${var.build_username}"
      build_password_encrypted = "${var.build_password_encrypted}"
    })
  }
  rhel_vm_name = "${var.vm_name}-RHEL"
}
