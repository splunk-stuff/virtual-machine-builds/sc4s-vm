# Build
build {
  name = "local"
  sources = [
    "virtualbox-iso.sc4s-rhel",
    "vmware-iso.sc4s-rhel",
  ]

  provisioner "shell" {
    execute_command = "echo '${var.build_password}' | sudo -S env {{ .Vars }} {{ .Path }}"
    scripts         = [
      // "scripts/virtualbox.sh",
      "scripts/vmware.sh"
    ]
  }

  provisioner "ansible" {
    user             = "${var.build_username}"
    playbook_file    = "../ansible/site-all.yml"
    ansible_env_vars = [
      "ANSIBLE_CONFIG=../ansible/ansible.cfg"
    ]
    extra_arguments  = [ "-v" ]
  }
}

build {
  name = "esxi"
  sources = [
    "vsphere-iso.sc4s-rhel"
  ]
  provisioner "ansible" {
    user             = "${var.build_username}"
    playbook_file    = "../ansible/site-all.yml"
    ansible_env_vars = [
      "ANSIBLE_CONFIG=../ansible/ansible.cfg"
    ]
    extra_arguments        = [ "-v" ]
  }

}
