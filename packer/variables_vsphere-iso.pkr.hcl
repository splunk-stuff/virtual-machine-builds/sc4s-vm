###
## vsphere-iso Variables #
#

variable "vsphere-iso_vcenter_server" {
  description = "The vcenter sever"
  type = string
}

variable "vsphere-iso_host" {
  description = "The esxi host"
  type = string
}

variable "vsphere-iso_username" {
  description = "The username to connect to the esxi host"
  type = string
}

variable "vsphere-iso_password" {
  description = "The password to connect to the esxi host"
  type = string
}

variable "vsphere-iso_insecure" {
  description = "The password to connect to the esxi host"
  type = bool
  default = false
}

variable "vsphere-iso_datastore" {
  description = "The datastore to use on the esxi host"
  type = string
}
