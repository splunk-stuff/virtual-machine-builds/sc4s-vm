## Required plugins
packer {
  required_plugins {
    virtualbox = {
      version = ">= 1.0.5"
      source = "github.com/hashicorp/virtualbox"
    }
    vmware = {
      version = ">= 1.0.10"
      source = "github.com/hashicorp/vmware"
    }
    vsphere = {
      version = ">= 1.2.2"
      source  = "github.com/hashicorp/vsphere"
    }
    ansible = {
      version = ">= 1.1.0"
      source  = "github.com/hashicorp/ansible"
    }
  }
}
