## Usage
From this directory (`packer`):

- `packer init .`
- `vi build_vars.auto.pkrvars.hcl` (update if required)
- Follow the instructions for a particular build

## Builds

### local

#### Virtualbox
`packer build -only="local.virtualbox-iso.sc4s-rhel" .`

#### VMware Workstation
`packer build -only="local.vmware-iso.sc4s-rhel" .`

### ESXI
`cp build_vars_vsphere-iso.auto.pkrvars.hcl.example build_vars_vsphere-iso.auto.pkrvars.hcl`

`vi build_vars_vsphere-iso.auto.pkrvars.hcl` (update with ESXI host details)

`packer build -only="esxi.vsphere-iso.sc4s-rhel" .`
