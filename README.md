# SC4S-VM

Build a SC4S VM with Packer or Vagrant. Ansible follows [SC4S Initial Setup](https://splunk.github.io/splunk-connect-for-syslog/main/gettingstarted/podman-systemd-general/).

## Usage
- Copy `ansible/roles/sc4s/defaults/main.yml` to `ansible/roles/sc4s/vars/main.yml`.
```
SC4S_DEST_SPLUNK_HEC_DEFAULT_URL: http://your.splunk.instance:8088
SC4S_DEST_SPLUNK_HEC_DEFAULT_TOKEN: xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx
SC4S_DEST_SPLUNK_HEC_DEFAULT_TLS_VERIFY: "no"
```
- Update with HEC config

### Packer
Build a VM from ISO for:

-  VirtualBox
-  VMware
-  ESXI

Follow the [instructions](/packer/README.md)

### Vagrant
Build a VagrantBox (VM) from `generic/centos9s` for:

-  VirtualBox (`vagrant up --provide=virtualbox`)
-  VMware (`vagrant up --provide=vmware_desktop`)
